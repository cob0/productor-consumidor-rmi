package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import interfaces.MensajesInterface;

public class ClienteProductor {

	public static void main(String[] args) {
		String nombreServicio;		
		Registry registry;
		MensajesInterface mensajes;
		String mensajeToSend = "";
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		
		try {
			registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido");
			
			nombreServicio = "Mensajes";
			
			mensajes = (MensajesInterface) registry.lookup(nombreServicio);
			while(!mensajeToSend.equalsIgnoreCase("salir")) {
				System.out.print("Escribe un mensaje: ");
				mensajeToSend = teclado.nextLine();
				mensajes.send(mensajeToSend);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
	}
	
}
