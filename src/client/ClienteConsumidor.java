package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import interfaces.MensajesInterface;

public class ClienteConsumidor {

	public static void main(String[] args) {
		String nombreServicio;		
		Registry registry;
		MensajesInterface mensajes;
		String mensajeToSend = "si";
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		
		try {
			registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido");
			
			nombreServicio = "Mensajes";
			
			mensajes = (MensajesInterface) registry.lookup(nombreServicio);
			while(mensajeToSend.equalsIgnoreCase("si")) {
				System.out.print("Pedir (si / no): ");
				mensajeToSend = teclado.nextLine();
				System.out.println(mensajes.receive());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
	}
}
