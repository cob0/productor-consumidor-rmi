package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MensajesInterface extends Remote {

	void send(Object obj) throws RemoteException;
	String receive() throws RemoteException;
	
}
