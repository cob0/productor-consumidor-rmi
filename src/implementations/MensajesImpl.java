package implementations;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.concurrent.LinkedBlockingQueue;

import interfaces.MensajesInterface;

public class MensajesImpl implements MensajesInterface, Serializable {

	private LinkedBlockingQueue<Object> mensajes;
	private static final long serialVersionUID = -6028439359359328282L;

	public MensajesImpl() throws RemoteException {
		this.setMensajes(new LinkedBlockingQueue<>());
	}
	
	public LinkedBlockingQueue<Object> getMensajes() {
		return mensajes;
	}

	public void setMensajes(LinkedBlockingQueue<Object> mensajes) {
		this.mensajes = mensajes;
	}
	
	@Override
	public void send(Object obj) throws RemoteException {
		if(obj != null)
			mensajes.add(obj);
	}

	@Override
	public String receive() throws RemoteException {
		String mensaje = null;
		
		try {
			mensaje = mensajes.take().toString();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return mensaje;
	}

}
